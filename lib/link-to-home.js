add_link_logo = function(link) {
    document.getElementById('logo').addEventListener('click', function() {
        location.href = link;
    }, false);
}

add_link_home = function(link) {
    const div = document.createElement('div');
    div.className = 'header';
    div.innerHTML = `
        <a href="https://ge-steven.github.io/">Steven Ge\n Itoh Laboratory\n(Home)</a>
    `;

    document.getElementsByClassName('reveal')[0].appendChild(div);
}