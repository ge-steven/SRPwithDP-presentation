let data_intr = {
  elements: {
    nodes: [
      { 
        data: { id: 'p1', 
                label: 'A', 
                position1: { x: 0, y: 0 }, 
                position2: { x: 0, y: 0 }, 
                position3: { x: 0, y: 0 },  
                position4: { x: 0, y: 0 }, 
                position5: { x: 0, y: 0 }, 
                backgroundColor: 'white' 
              } 
      },
      { 
        data: { id: 'p2', 
                label: 'B', 
                position1: { x: 0, y: 50 }, 
                position2: { x: 0, y: 70 }, 
                position3: { x: 50, y: 0 }, 
                position4: { x: 50, y: 0 }, 
                position5: { x: 50, y: 0 }, 
                backgroundColor: 'white' 
              } 
      },
      { data: { id: 'p3', 
                label: 'C', 
                position1: { x: 0, y: 100 }, 
                position2: { x: 50, y: 70 }, 
                position3: { x: 50, y: 70 }, 
                position4: { x: 100, y: 0 }, 
                position5: { x: 0, y: 70 }, 
                backgroundColor: 'white' 
              } 
      },
      { data: { id: 'p4', 
                label: 'D', 
                position1: { x: 50, y: 0 }, 
                position2: { x: 50, y: 0 }, 
                position3: { x: 0, y: 70 }, 
                position4: { x: 0, y: 70 }, 
                position5: { x: 100, y: 0 }, 
                backgroundColor: 'white' 
              } 
      },
      { data: { id: 'p5', 
                label: 'E', 
                position1: { x: 50, y: 50 }, 
                position2: { x: 110, y: 0 }, 
                position3: { x: 110, y: 0 }, 
                position4: { x: 50, y: 70 }, 
                position5: { x: 50, y: 70 }, 
                backgroundColor: 'white' 
              } 
      },
      { data: { id: 'p6', 
                label: 'F', 
                position1: { x: 50, y: 100 }, 
                position2: { x: 110, y: 70 }, 
                position3: { x: 110, y: 70 }, 
                position4: { x: 100, y: 70 }, 
                position5: { x: 100, y: 70 }, 
                backgroundColor: 'white' 
              } 
      }
    ]
  }
}

let data_rdp = {
  elements: {
    nodes: [
      { 
        data: { id: 'p1', 
                label: 'A', 
                position1: { x: 0, y: 0 }, 
                position2: { x: 0, y: 0 }, 
                backgroundColor: 'red' 
              } 
      },
      { 
        data: { id: 'p2', 
                label: 'B', 
                position1: { x: 0, y: 50 }, 
                position2: { x: 50, y: 0 }, 
                backgroundColor: 'red' 
              } 
      },
      { data: { id: 'p3', 
                label: 'C', 
                position1: { x: 0, y: 100 }, 
                position2: { x: 100, y: 0 }, 
                backgroundColor: 'red' 
              } 
      },
      { data: { id: 'p4', 
                label: 'D', 
                position1: { x: 50, y: 0 }, 
                position2: { x: 0, y: 70 }, 
                backgroundColor: 'red' 
              } 
      },
      { data: { id: 'p5', 
                label: 'E', 
                position1: { x: 50, y: 50 }, 
                position2: { x: 50, y: 70 }, 
                backgroundColor: 'blue' 
              } 
      },
      { data: { id: 'p6', 
                label: 'F', 
                position1: { x: 50, y: 100 }, 
                position2: { x: 100, y: 70 },  
                backgroundColor: 'blue' 
              } 
      }
    ]
  }
}

let data_eqcon = {
  elements: {
    nodes: [
      { 
        data: { id: 'c1', 
                label: '', 
                position1: { x: 0, y: 0 }, 
                backgroundColor: 'white' 
              } 
      },
      { 
        data: { id: 'c2', 
                label: '', 
                position1: { x: 0, y: 0 }, 
                backgroundColor: 'white' 
              } 
      },
      { 
        data: { id: 'p1', 
                label: 'A', 
                position1: { x: 0, y: 0 }, 
                backgroundColor: 'red',
                parent: 'c1'
              } 
      },
      { 
        data: { id: 'p2', 
                label: 'B', 
                position1: { x: 50, y: 0 }, 
                backgroundColor: 'red' ,
                parent: 'c1'
              } 
      },
      { data: { id: 'p3', 
                label: 'C', 
                position1: { x: 100, y: 0 }, 
                backgroundColor: 'red' ,
                parent: 'c1'
              } 
      },
      { data: { id: 'p4', 
                label: 'D', 
                position1: { x: 0, y: 70 }, 
                backgroundColor: 'red' ,
                parent: 'c2'
              } 
      },
      { data: { id: 'p5', 
                label: 'E', 
                position1: { x: 50, y: 70 }, 
                backgroundColor: 'blue' ,
                parent: 'c2'
              } 
      },
      { data: { id: 'p6', 
                label: 'F', 
                position1: { x: 100, y: 70 }, 
                backgroundColor: 'blue' ,
                parent: 'c2'
              } 
      }
    ]
  }
}