var cy_future = cytoscape({
  container: document.getElementById('graph1-cy_future'),

  style: [
    {
      selector: 'node',
      style: {
        'label': 'data(label)',
        'text-valign': 'center',
        'text-halign': 'center',
        'background-color': 'data(backgroundColor)',
        'border-width': 2,
        'border-color': 'black'
      }
    },

    {
      selector: 'node:parent',
      style: {
        'shape': 'roundrectangle',
        'opacity': 0.75,
        'label': '',
        'background-color': 'grey',
        'border-color': 'white'
      }
    },

    {
      selector: 'edge',
      style: {
        'curve-style': 'bezier',
        'target-arrow-shape': 'triangle'
      }
    },

    {
      selector: '.cdnd_future-grabbed-node',
      style: {
        'background-color': 'orange'
      }
    },

    {
      selector: '.cdnd_future-drop-sibling',
      style: {
        'background-color': 'orange'
      }
    },

    {
      selector: '.cdnd_future-drop-target',
      style: {
        'border-color': 'orange',
        'border-style': 'dashed'
      }
    }
  ],
  layout: {
    name: 'preset',
    zoom : 2.5,
    pan: { x: 311, y: 97 },
    fit: true,
    positions: function(node){return node._private.data.position1}
  },

  elements: data_eqcon.elements
});

var cdnd_future = cy_future.compoundDragAndDrop();
var removeEmptyParents_future = true;

var isParentOfOneChild_future = function(node){
  return node.isParent() && node.children().length === 1;
};

var removeParent_future = function(parent){
  parent.children().move({ parent: null });
  parent.remove();
};

var removeParent_futuresOfOneChild = function(){
  cy_future.nodes().filter(isParentOfOneChild_future).forEach(removeParent_future);
};

// custom handler to remove parents with only 1 child on drop
cy_future.on('cdnd_futureout', function(event, dropTarget){
  if( removeEmptyParents_future && isParentOfOneChild_future(dropTarget) ){
    removeParent_future(dropTarget);
  }
});

// custom handler to remove parents with only 1 child (on remove of drop target or drop sibling)
cy_future.on('remove', function(event){
  if( removeEmptyParents_future ){
    removeParent_futuresOfOneChild();
  }
});

var layoutanimation_future = function(i) {
  return cy_future.layout(
    {
      name: 'preset',
      positions: function(node){return node._private.data["position" + i]}, // map of (node id) => (position obj); or function(node){ return somPos; }
      zoom: 2.5, // the zoom level to set (prob want fit = false if set)
      pan: undefined, // the pan level to set (prob want fit = false if set)
      fit: false, // whether to fit to viewport
      animate: true, // whether to transition the node positions
      animationDuration: 600, // duration of animation in ms if enabled
      animationEasing: 'ease-in-out-sine', // easing of animation if enabled
      animateFilter: function ( node, i ){ return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
      ready: undefined, // callback on layoutready
      stop: undefined, // callback on layoutstop
      transform: function (node, position ){ return position; } // transform a given node position. Useful for changing flow direction in discrete layouts
    }
  ).start();
}