var cy_intr = cytoscape({
  container: document.getElementById('graph1-cy_intr'),

  style: [
    {
      selector: 'node',
      style: {
        'label': 'data(label)',
        'text-valign': 'center',
        'text-halign': 'center',
        'background-color': 'data(backgroundColor)',
        'border-width': 2,
        'border-color': 'black'
      }
    },

    {
      selector: 'node:parent',
      style: {
        'shape': 'roundrectangle',
        'opacity': 0,
        'label': '',
        'background-color': 'grey',
        'border-color': 'white'
      }
    },

    {
      selector: 'edge',
      style: {
        'curve-style': 'bezier',
        'target-arrow-shape': 'triangle'
      }
    },

    {
      selector: '.cdnd_intr-grabbed-node',
      style: {
        'background-color': 'orange'
      }
    },

    {
      selector: '.cdnd_intr-drop-sibling',
      style: {
        'background-color': 'orange'
      }
    },

    {
      selector: '.cdnd_intr-drop-target',
      style: {
        'border-color': 'orange',
        'border-style': 'dashed'
      }
    }
  ],
  layout: {
    name: 'preset',
    zoom : 2.5,
    pan: { x: 311, y: 97 },
    fit: true,
    positions: function(node){return node._private.data.position1}
  },

  elements: data_intr.elements
});

var cdnd_intr = cy_intr.compoundDragAndDrop();
var removeEmptyParents_intr = true;

var isParentOfOneChild_intr = function(node){
  return node.isParent() && node.children().length === 1;
};

var removeParent_intr = function(parent){
  parent.children().move({ parent: null });
  parent.remove();
};

var removeParent_intrsOfOneChild = function(){
  cy_intr.nodes().filter(isParentOfOneChild_intr).forEach(removeParent_intr);
};

// custom handler to remove parents with only 1 child on drop
cy_intr.on('cdnd_introut', function(event, dropTarget){
  if( removeEmptyParents_intr && isParentOfOneChild_intr(dropTarget) ){
    removeParent_intr(dropTarget);
  }
});

// custom handler to remove parents with only 1 child (on remove of drop target or drop sibling)
cy_intr.on('remove', function(event){
  if( removeEmptyParents_intr ){
    removeParent_intrsOfOneChild();
  }
});

var layoutanimation_intr = function(i) {
  return cy_intr.layout(
    {
      name: 'preset',
      positions: function(node){return node._private.data["position" + i]}, // map of (node id) => (position obj); or function(node){ return somPos; }
      zoom: 2.5, // the zoom level to set (prob want fit = false if set)
      pan: undefined, // the pan level to set (prob want fit = false if set)
      fit: false, // whether to fit to viewport
      animate: true, // whether to transition the node positions
      animationDuration: 600, // duration of animation in ms if enabled
      animationEasing: 'ease-in-out-sine', // easing of animation if enabled
      animateFilter: function ( node, i ){ return true; }, // a function that determines whether the node should be animated.  All nodes animated by default on animate enabled.  Non-animated nodes are positioned immediately when the layout starts
      ready: undefined, // callback on layoutready
      stop: undefined, // callback on layoutstop
      transform: function (node, position ){ return position; } // transform a given node position. Useful for changing flow direction in discrete layouts
    }
  ).start();
}

var addparents_intr = function(agent_ids_list) {
  for(i = 0; i < agent_ids_list.length; i++) {
    try {
      cy_intr.add( { data: { id: 'c'+i, label: '', opacity: 0 }, position: { x: -100, y: -100 } } );
      agent_ids_list[i].forEach(element => cy_intr.nodes()[element].move({parent: 'c'+i}));
      let selected = cy_intr.getElementById("c"+i);
      var jAni = selected.animation({
        style: { 'opacity': 0.75 },
        duration: 600,
        easing: 'ease-in-out-sine'
      });
      jAni.play()
    } catch (error) {
      // When entering presentation not at the start
      console.log("Entered presentation not at the start")
    }
  }
}

var removeparents_intr = function(rem_nr, agent_ids_list) {
  var i = 0;
  while(i < rem_nr) {
    try{
      let selected = cy_intr.getElementById("c"+i);
      var jAni = selected.animation({
        style: { 'opacity': 0 },
        duration: 600,
        easing: 'ease-in-out-sine'
      });
      jAni.play().promise().then(function(){
        selected.children().move({parent : (selected.parent().id() ? selected.parent().id() : null)});
        selected.remove();
        addparents_intr(agent_ids_list);
      });
    } catch(error) {
      // When entering presentation not at the start
      i = i + 1;
      console.log("Entered presentation not at the start");
      addparents_intr(agent_ids_list);
    }
    i = i + 1;
  }

  if(rem_nr == 0) {
    addparents_intr(agent_ids_list);
  }
}


var cy_intrtoEventHandler = function(event) {
  if(event.fragment.id === undefined) {
  }
  else if (event.fragment.id == 'graph1-cy_intr') {
    layoutanimation_intr(1);
  }
  else if (event.fragment.id == 'graph2-cy_intr') {
    layoutanimation_intr(2);
  }
  else if (event.fragment.id == 'graph3-cy_intr') {
    removeparents_intr(0, [[0,3], [1,2], [4,5]]);
  }
  else if (event.fragment.id == 'graph4-cy_intr') {
    layoutanimation_intr(3);
  }
  else if (event.fragment.id == 'graph5-cy_intr') {
    removeparents_intr(3, [[0,1], [3,2], [4,5]]);
  }
  else if (event.fragment.id == 'graph6-cy_intr') {
    layoutanimation_intr(4);
  }
  else if (event.fragment.id == 'graph7-cy_intr') {
    removeparents_intr(3, [[0,1,2], [3,4,5]]);
  }
  else if (event.fragment.id == 'graph8-cy_intr') {
    layoutanimation_intr(5);
  }
  else if (event.fragment.id == 'graph9-cy_intr') {
    removeparents_intr(2, [[0,1,3], [2,4,5]]);
  }
}

Reveal.addEventListener('fragmentshown', cy_intrtoEventHandler);

var cy_intrtoEventHandlerBack = function(event) {
  if(event.fragment.id === undefined) {
  }
  else if (event.fragment.id == 'graph1-cy_intr') {
    layoutanimation_intr(1);
  }
  else if (event.fragment.id == 'graph2-cy_intr') {
    layoutanimation_intr(1);
  }
  else if (event.fragment.id == 'graph3-cy_intr') {
    removeparents_intr(3, []);
  }
  else if (event.fragment.id == 'graph4-cy_intr') {
    layoutanimation_intr(2);
  }
  else if (event.fragment.id == 'graph5-cy_intr') {
    removeparents_intr(3, [[0,3], [1,2], [4,5]]);
  }
  else if (event.fragment.id == 'graph6-cy_intr') {
    layoutanimation_intr(3);
  }
  else if (event.fragment.id == 'graph7-cy_intr') {
    removeparents_intr(3, [[0,1], [3,2], [4,5]]);
  }
  else if (event.fragment.id == 'graph8-cy_intr') {
    layoutanimation_intr(4);
  }
  else if (event.fragment.id == 'graph9-cy_intr') {
    removeparents_intr(3, [[0,1,2], [3,4,5]]);
  }
}


Reveal.addEventListener('fragmenthidden', cy_intrtoEventHandlerBack);