document.addEventListener("fragmentshown", function(event) {
    if (event.fragment.id === 'draw-graph-svg') {
        new Vivus('draw-graph-svg', 
        { type: 'delayed', duration: 450, pathTimingFunction: Vivus.EASE, animTimingFunction: Vivus.EASE, file: 'images/logo-black.svg', reverseStack: false },
        function doDone(obj) {
            obj.el.classList.add('finished');
        });
    }
});

document.addEventListener("fragmenthidden", function(event) {
    if (event.fragment.id === 'draw-graph-svg') {
        document.getElementById('draw-graph-svg').innerHTML = "";
    }
});